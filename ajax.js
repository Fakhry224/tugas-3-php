function submitForm() {
  const formData = new FormData(document.getElementById("dataForm"));
  const url = "ajax.php";

  axios
    .post(url, formData)
    .then((response) => {
      alert("Berhasil mengambil data");
      document.getElementById("result").innerHTML =
        "Response : </br>" + response.data;
    })
    .catch((error) => {
      console.error("Error fetching data: ", error);
    });
}
