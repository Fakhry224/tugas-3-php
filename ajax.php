<?php
    if ($_SERVER['REQUEST_METHOD'] == "POST"){
        if(isset($_POST['prodi'], $_POST['name'], $_POST['nim'], $_POST['gender'])) {
            $name = $_POST['name'];
            $nim = $_POST['nim'];
            $prodi = $_POST['prodi'];
            $gender = $_POST['gender'];

            echo "Nama : " . $name . "<br>";
            echo "NIM : " . $nim . "<br>";
            echo "Prodi : " . $prodi . "<br>";
            echo "Gender : " . $gender . "<br>";
        } else {
            echo "Missing Data";
        }
    }  else {
        echo "This script only handles POST requests.";
    }
?>
